'use strict';

angular
  .module('myAlbumApp', [
    'ngResource',
    'ngRoute',
    'ngFacebook'
  ])
  .config(['$routeProvider', '$facebookProvider', function($routeProvider, $facebookProvider) {
    $routeProvider
      .when('/albums', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/albums/:albumId',{
        templateUrl: 'views/album.html',
        controller: 'AlbumCtrl',
        reloadOnSearch: false
      })
      .otherwise({
        redirectTo: '/albums'
      });
    $facebookProvider.setAppId('1444802179098086');
    $facebookProvider.setPermissions('email,user_likes,user_photos');
  }])
  .run(['$rootScope', '$location', '$route', 'AuthService', function($rootScope, $location, $route, Auth) {

    $rootScope.auth = Auth;
    $rootScope.prevLoggedIn = Auth.loggedIn;
    Auth.refresh();

    $rootScope.$watch('auth.loggedIn', function() {
      if(!$rootScope.auth.loggedIn && $rootScope.prevLoggedIn == true){
        $route.reload();
      }
      $rootScope.prevLoggedIn = Auth.loggedIn;
    });

    $rootScope.go = function(path) {
      $location.path(path);
    };
  }]);

