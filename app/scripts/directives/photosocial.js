'use strict';

angular.module('myAlbumApp')
  .directive('photosocial', function () {
    return {
      templateUrl: 'views/photosocial.html',
      restrict: 'E',
      scope: "=photo",
      link: function(scope, element, attrs) {
        element.height($('.photo-container').height());
      }
    };
  });
