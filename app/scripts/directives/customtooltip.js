'use strict';

angular.module('myAlbumApp')
  .directive('customTooltip', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        element.tooltip({
          html: true,
          placement: 'top',
          title: function() {
            return $("#" + attrs.customTooltip).html();
          }
        });
      }
    };
  });
