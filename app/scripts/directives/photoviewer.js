'use strict';

angular.module('myAlbumApp')
  .directive('photoviewer', function () {
    return {
      templateUrl: 'views/photoviewer.html',
      restrict: 'A',
      scope: '=',
      link: function(scope, element, attrs) {
        // scope.$watch('photo', function() {
        //   var img = $('#photo-img');
        //   img.load(function() {
        //     var next = $('#photo-next');
        //     var prev = $('#photo-previous');
        //     next.width(img.width() + 'px');
        //     next.height(img.height() + 'px');
        //   });
        // });
      }
    };
  })
  .directive('imageonload', ['$timeout', function($timeout) {
      function resetArrows(){
        var img = $('#photo-img');
        var next = $('#photo-next');
        var prev = $('#photo-previous');
        var social = $('.photo-social');
        var newWidth = (img.width() / 2.0) + 'px';
        var newHeight = img.height() + 'px';
        next.width(newWidth);
        next.height(newHeight);
        prev.width(newWidth);
        prev.height(newHeight);
        prev.css({marginLeft: '-' + newWidth});
        social.height(img.height());
        social.css({maxWidth: img.width() + 'px'})
      }
  
      return{
        restrict: 'A',
        link: function(scope, element, attrs) {
          element.bind('load', function() {
            if(element.height() == 0)
              $timeout(function() {
                resetArrows();
              }, 1000);
            else
              resetArrows();
          });
        }
      }
    }]);
