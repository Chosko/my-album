'use strict';

angular.module('myAlbumApp')
  .directive('showModal', ['LazyDataService', function (LazyData) {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        scope.$watch('photoViewer', function(newValue, oldValue) {
          if(oldValue != newValue){
            if(newValue)
              element.modal('show');
            else
              element.modal('hide');
          }
        });

        element.on('show.bs.modal', function(e) {
          scope.openPhotoViewer();
        });

        element.on('hide.bs.modal', function(e) {
          scope.closePhotoViewer();
        });

        scope.$on('$routeChangeStart', function(next, current) {
          element.modal('hide');
        });
      }
    };
  }]);
