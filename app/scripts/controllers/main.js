'use strict';

angular.module('myAlbumApp')
  .controller('MainCtrl',[
    '$scope',
    '$location',
    'LazyDataService',
    'AuthService',
    function($scope, $location, LazyData, Auth){
    
    $scope.auth = Auth;
    $scope.basePath = $location.path();
    $scope.title = "Your albums";
    Auth.onrefresh(function() {$scope.getAlbums();});
    
    $scope.getAlbums = function() {
      LazyData.getAlbums()
        .then(function(response){
          $scope.albums = response;
        })
        .then(function(){
          LazyData.injectAlbumsThumbs();
        });
    }

    if(Auth.loggedIn) $scope.getAlbums();
  }]);