'use strict';

angular.module('myAlbumApp')
  .controller('AlbumCtrl',[
    '$scope',
    '$location',
    '$routeParams',
    '$q',
    'LazyDataService',
    'AuthService',
    function($scope, $location, $routeParams, $q, LazyData, Auth){
    
    $scope.auth = Auth;
    $scope.basePath = $location.path();
    $scope.photoViewer = false;
    Auth.onrefresh(function() {$scope.getAlbum();});
    
    $scope.getAlbum = function() {
      LazyData.getAlbum($routeParams.albumId)
        .then(function(response) {
          $scope.album = response;
          $scope.title = $scope.album.name;
        })
        .then(function(response) {
          LazyData.getPhotos($scope.album.id).then(function(response) {
            $scope.photos = response;
          });
        })
        .then(function() {
          if($routeParams.photoId){
            $scope.loadPhoto($scope.album.id, $routeParams.photoId)
              .then(function() {
                $scope.openPhotoViewer();
              });
          }
        });
    };

    $scope.loadPhoto = function(albumId, photoId){
      var deferred = $q.defer();
      LazyData.getPhoto(albumId, photoId)
        .then(function(response) {
          $scope.photo = response;
          $scope.album = LazyData.currentAlbum;
          deferred.resolve();
        });
      return deferred.promise;
    }

    $scope.getPhoto = function(albumId, photoId) {
      $scope.loadPhoto(albumId, photoId).then(function() {
        $scope.setUrl();
      });
    };

    $scope.setUrl = function(){
      $location.search('photoId', $scope.photo.id);
    };

    $scope.unsetUrl = function() {
      $location.search('photoId', null);
    };

    $scope.openPhotoViewer = function(){
      $scope.photoViewer = true;
      $scope.setUrl();
    };

    $scope.closePhotoViewer = function() {
      $scope.photoViewer = false;
      $scope.unsetUrl();
    };

    if(Auth.loggedIn) $scope.getAlbum();
  }]);
