'use strict';

angular.module('myAlbumApp')
  .factory('AuthService', ['$facebook', function ($facebook) {
    var fact = {
      user: {},
      loggedIn: false,
      accessToken: false,

      login: function(){
        $facebook.login().then(function(){
          fact.refresh();
        });
      },

      logout: function(){
        $facebook.logout().then(function(){
          fact.refresh();
        });
      },

      connectedCallback: function() {},

      disconnectedCallback: function() {},

      onrefresh: function(connectedCallback, disconnectedCallback){
        fact.connectedCallback = connectedCallback || fact.connectedCallback;
        fact.disconnectedCallback = disconnectedCallback || fact.disconnectedCallback;
      },

      refresh: function(){
        $facebook.getLoginStatus()
          .then(
            function(response) {
              if(response.status === "connected")
              {
                fact.loggedIn = true;
                fact.accessToken = response.authResponse.accessToken;
                fact.updateUserInfo();
                fact.connectedCallback();
                return true;
              }
              else
              {
                fact.loggedIn = false;
                fact.user = {};
                fact.accessToken = false;
                fact.disconnectedCallback();
                return false;
              }
            }
          );
      },

      updateUserInfo: function() {
        $facebook.api("/me").then(
          function(response){
            fact.user = response;
          }
        ).then(function() {
          $facebook.api("/me/picture?height=115&width=115").then(
            function(response) {
              fact.user.picture = response.data.url;
            }
          );

          $facebook.api("me?fields=cover").then(
            function(response){
              fact.user.cover = response.cover.source;
            }
          );
        });
      },
    }

    return fact;
  }]);
