'use strict';

angular.module('myAlbumApp')
  .factory('LazyDataService', ['$facebook', '$q', '$http', function ($facebook, $q, $http) {
    var fact = {
      
      albumsReady: false,

      albumsThumbsReady: false,
      
      albums: [],

      albumsIndex: [],

      albumsThumbsIndex: [],

      photosIndex: [],

      currentAlbum: false,
      
      getAlbums: function(){
        var deferred = $q.defer();

        if(fact.albumsReady){
          deferred.resolve(fact.albums);
        }
        else{
          $facebook.api('/me?fields=albums.fields(id,created_time,cover_photo,name,likes.summary(true),comments.summary(true))&limit=1000').then(
            function(response) {
              fact.albums = response.albums.data;
              deferred.resolve(fact.albums);
              for (var i = 0; i < fact.albums.length; i++) {
                fact.albumsIndex[fact.albums[i].id] = i;
              };
              fact.albumsReady = true;
            },
            function(err) {
              deferred.reject(err);
            }
          );
        }
        return deferred.promise;
      },

      injectAlbumsThumbs: function(){
        if(!fact.albumsThumbsReady){
          fact.albumsThumbsIndex = [];
          for (var i = 0; i < fact.albums.length; i++) {
            var coverId = fact.albums[i].cover_photo;
            if(coverId){
              fact.albumsThumbsIndex[coverId] = i;
              $facebook.api('/' + coverId).then(
                function(response) {
                  var img = response.images[0].source;
                  for (var i = 0; i < response.images.length; i++) {
                    if(response.images[i].height < 200)
                    {
                      img = response.images[i].source;
                      break;
                    }
                  };
                  fact.albums[fact.albumsThumbsIndex[response.id]].thumb = img;
                },
                function(err){
                  console.error(err)
                }
              );
            }
          }
          fact.albumsThumbsReady = true;
        }
      },

      getAlbum: function(albumId){
        var deferred = $q.defer();

        if(fact.currentAlbum && fact.currentAlbum.id == albumId){
          deferred.resolve(fact.currentAlbum);
        }
        else if(fact.albumsIndex[albumId]){
          fact.currentAlbum = fact.albums[fact.albumsIndex[albumId]];
          deferred.resolve(fact.currentAlbum);
        }
        else{
          $facebook.api('/' + albumId).then(
            function(response) {
              fact.currentAlbum = response;
              deferred.resolve(fact.currentAlbum);
              fact.albumsIndex[albumId] = fact.albums.length;
              fact.albums.push(fact.currentAlbum);
              fact.currentAlbum.ready = false;
            },
            function(err) {
              console.error(err);
            }
          );
        }

        return deferred.promise;
      },

      getPhotos: function(albumId){
        var deferred = $q.defer();

        if(!(fact.currentAlbum && fact.currentAlbum.id == albumId)){
          var errMsg = '[LazyDataService ERROR] getPhotos(albumId) called when the current album was not ready yet. Please call getAlbum(albumId) before.';
          console.error(errMsg);
          deferred.reject(errMsg);
        }
        else if(fact.currentAlbum.ready){
          deferred.resolve(fact.currentAlbum.images);
        }
        else{
          var albumImgs = [];
          $facebook.api('/' + albumId + "/photos?fields=id,images,created_time,images,name,source,link,likes.summary(true),comments.summary(true)&limit=1000").then(
            function(response) {
              fact.currentAlbum.images = response.data;
              deferred.resolve(fact.currentAlbum.images);
              for (var i = 0; i < fact.currentAlbum.images.length; i++) {
                var imgs = fact.currentAlbum.images[i].images;
                var img = imgs[0];
                for (var j = 0; j < imgs.length; j++) {
                  if (imgs[j].height < 200) {
                    img = imgs[j].source;
                    break;
                  }
                };
                fact.currentAlbum.images[i].thumb = img;
                fact.photosIndex[fact.currentAlbum.images[i].id] = i;
              };
              fact.currentAlbum.ready = true;
            },
            function(err) {
              console.error(err);
            }
          );
        }

        return deferred.promise;
      },

      getPhoto: function(albumId, photoId){
        var deferred = $q.defer();
        if(!(fact.currentAlbum && fact.currentAlbum.id == albumId)){
          fact.getAlbum(albumId).then(function() {
            deferred.resolve(fact.getPhoto(albumId, photoId));
          });
        }
        else if (!fact.currentAlbum.ready) {
          fact.getPhotos(albumId).then(function() {
            deferred.resolve(fact.getPhoto(albumId, photoId));
          });
        }
        else{
          var index = fact.photosIndex[photoId];
          var img = fact.currentAlbum.images[index];
          deferred.resolve(img);
          if(index > 0){
            img.previous = fact.currentAlbum.images[index-1];
            $http({method: 'GET', url: img.previous.source, cache: true})
          }
          if(index < fact.currentAlbum.images.length-1)
          {
            img.next = fact.currentAlbum.images[index+1];
            $http({method: 'GET', url: img.next.source, cache: true})
          }
          img.cached = true;
        }
        return deferred.promise;
      },
    }

    return fact;
  }]);
